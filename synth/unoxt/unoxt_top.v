`timescale 1ns / 1ns
`default_nettype wire

//    This file is part of the ZXUNO Spectrum core. 
//    Creation date is 02:28:18 2014-02-06 by Miguel Angel Rodriguez Jodar
//    (c)2014-2020 ZXUNO association.
//    ZXUNO official repository: http://svn.zxuno.com/svn/zxuno
//    Username: guest   Password: zxuno
//    Github repository for this core: https://github.com/mcleod-ideafix/zxuno_spectrum_core
//
//    ZXUNO Spectrum core is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ZXUNO Spectrum core is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//e
//    You should have received a copy of the GNU General Public License
//    along with the ZXUNO Spectrum core.  If not, see <https://www.gnu.org/licenses/>.
//
//    Any distributed copy of this file must keep this notice intact.

//Max
//UnoXT Generation Selector
`define unoxt
//`define unoxt2

`ifdef unoxt
	module unoxt_top (
`elsif unoxt2
	module unoxt2_top (
`else
	module unoxt_top (
`endif

   input wire clock_50_i,

   output wire [2:0] rgb_r_o,
   output wire [2:0] rgb_g_o,
   output wire [2:0] rgb_b_o,
   output wire hsync_o,
   output wire vsync_o,
   input wire ear_port_i,
   output wire mic_port_o,
   inout wire ps2_clk_io,
   inout wire ps2_data_io,
   inout wire ps2_pin6_io,
   inout wire ps2_pin2_io,
   output wire audioext_l_o,
   output wire audioext_r_o,

   inout wire esp_gpio0_io,
   inout wire esp_gpio2_io,
	output wire esp_tx_o,
   input wire esp_rx_i,
 
   output wire [20:0] ram_addr_o,
	output wire ram_lb_n_o,
	output wire ram_ub_n_o,
   inout wire [15:0] ram_data_io,
   output wire ram_oe_n_o,
   output wire ram_we_n_o,
   output wire ram_ce_n_o,
   
   output wire flash_cs_n_o,
   output wire flash_sclk_o,
   output wire flash_mosi_o,
   input wire flash_miso_i,
	output wire flash_wp_o,
	output wire flash_hold_o,
   
   input wire joyp1_i,
   input wire joyp2_i,
   input wire joyp3_i,
   input wire joyp4_i,
   input wire joyp6_i,
   output wire joyp7_o,
   input wire joyp9_i,
	
   input wire btn_divmmc_n_i,
   input wire btn_multiface_n_i,

   output wire sd_cs0_n_o,    
   output wire sd_sclk_o,     
   output wire sd_mosi_o,    
   input wire sd_miso_i,

	output wire led_red_o,
	output wire led_yellow_o,
   output wire led_green_o,   // nos servir como testigo de uso de la SPI
	output wire led_blue_o,

	inout wire [8:0] test

   );
   
   //Max
	`define DEFAULT_100_LEDS;
	
	`ifdef ROUND_DIF_LEDS
		//3mm round diffused LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd50;
		localparam ledGreenK = 16'd12;
		localparam ledBlueK = 16'd50;
	`elsif SQUARE_LEDS
		//square color LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd33;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd20;
	`else
		//default 100% LEDs
		localparam ledRedK = 16'd100;
		localparam ledYellowK = 16'd100;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd100;
	`endif
	
	wire greenLed;
	wire sysclk;
	wire ram_ce_oe_n_o;

	zxuno_top_vga2m 
	#(.per_opll_g(1), .per_jt51_g(0) )
	msx1(
		.clock_50_i(clock_50_i),
		//Max
		.clock21m(sysclk),
		
		.key_nmi_n_i(btn_divmmc_n_i),
		.key_service_n_i(btn_multiface_n_i),

		.sram_addr_o(ram_addr_o),
		.sram_data_io(ram_data_io[7:0]),
		
		.sram_we_n_o(ram_we_n_o),
		.sram_ceoe_n_o(ram_ce_oe_n_o),
		.ps2_clk_io(ps2_clk_io),
		.ps2_data_io(ps2_data_io),
		
		.sd_cs_n_o(sd_cs0_n_o),
		.sd_sclk_o(sd_sclk_o),
		.sd_mosi_o(sd_mosi_o),
		.sd_miso_i(sd_miso_i),
		
		.flash_cs_n_o(flash_cs_n_o),
		.flash_sclk_o(flash_sclk_o),
		.flash_mosi_o(flash_mosi_o),
		.flash_miso_i(flash_miso_i),
		.flash_wp_o(flash_wp_o),
		.flash_hold_o(flash_hold_o),

		.joy1_up_i(joyp1_i),
		.joy1_down_i(joyp2_i),
		.joy1_left_i(joyp3_i),
		.joy1_right_i(joyp4_i),
		.joy1_fire1_i(joyp6_i),
		.joy1_fire2_i(joyp9_i),
		
		.joy2_up_i(1'b1),
		.joy2_down_i(1'b1),
		.joy2_left_i(1'b1),
		.joy2_right_i(1'b1),
		.joy2_fire1_i(1'b1),
		.joy2_fire2_i(1'b1),

		.dac_l_o(audioext_l_o),
		.dac_r_o(audioext_r_o),
		.ear_i(ear_port_i),
		
		.vga_r_o(rgb_r_o),
		.vga_g_o(rgb_g_o),
		.vga_b_o(rgb_b_o),
		.vga_csync_n_o(hsync_o),
		.vga_vsync_n_o(vsync_o),
		
		.led_o(greenLed)
	);
 
 	assign joyp7_o = 1'b1;
	assign ram_oe_n_o = ram_ce_oe_n_o;
	assign ram_ce_n_o = ram_ce_oe_n_o;
	assign ram_lb_n_o = 1'b0;
	assign ram_ub_n_o = 1'b1;
	assign ram_data_io[15:8] = 8'bz;
	
	assign mic_port_o = 1'b1;
 
	assign esp_gpio0_io = 1'b1;
   assign esp_gpio2_io = 1'b1;
	assign esp_tx_o = 1'b1;

 	ledPWM ledRed(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b1),
		.y1(16'd100),
		.y2(ledRedK),	
		.led(led_red_o)
    );

	ledPWM ledYellow(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b0),
		.y1(16'd100),
		.y2(ledYellowK),	
		.led(led_yellow_o)
    );

	ledPWM ledGreen(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(greenLed),
		.y1(16'd100),
		.y2(ledGreenK),	
		.led(led_green_o)
    );

	ledPWM ledBlue(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b0),
		.y1(16'd100),
		.y2(ledBlueK),	
		.led(led_blue_o)
    );
  
endmodule
